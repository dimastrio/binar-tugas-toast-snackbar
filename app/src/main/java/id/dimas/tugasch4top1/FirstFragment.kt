package id.dimas.tugasch4top1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import id.dimas.tugasch4top1.databinding.FragmentFirstBinding


class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showName()
        showSnackbar()
    }

    private fun showName() {
        binding.btnShowName.setOnClickListener {
            val name = binding.edtName.text.toString()
            Toast.makeText(requireContext(), "Selamat Datang $name", Toast.LENGTH_LONG).show()
        }
    }

    private fun showSnackbar() {
        binding.btnSnackbar.setOnClickListener {

            Snackbar.make(it, "Kamu bosan?", Snackbar.LENGTH_INDEFINITE)
                .setAction("Iya") {
                    Toast.makeText(
                        requireContext(),
                        "Belajar aja biar tambah pinter",
                        Toast.LENGTH_SHORT
                    ).show()
                }.addAction(R.layout.snackbar_button, "Tidak") {
                    Toast.makeText(
                        requireContext(),
                        "Oke bagus",
                        Toast.LENGTH_SHORT
                    ).show()
                }.show()
        }
    }

    fun Snackbar.addAction(
        @LayoutRes aLayoutId: Int,
        aLabel: String,
        aListener: View.OnClickListener?
    ): Snackbar {
        // Add our button
        val button = LayoutInflater.from(view.context).inflate(aLayoutId, null) as Button
        // Using our special knowledge of the snackbar action button id we can hook our extra button next to it
        view.findViewById<Button>(com.google.android.material.R.id.snackbar_action).let {
            // Copy layout
            button.layoutParams = it.layoutParams
            // Copy colors
            (button as? Button)?.setTextColor(it.textColors)
            (it.parent as? ViewGroup)?.addView(button)
        }
        button.text = aLabel
        button.setOnClickListener { this.dismiss(); aListener?.onClick(it) }
        return this
    }


}